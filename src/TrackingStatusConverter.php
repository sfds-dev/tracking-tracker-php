<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace TrackingTracker;


class TrackingStatusConverter {
    public function convert($data) {
        $result = array();

        if (!$data) {
            return $result;
        }

        if (array_key_exists('hits', $data)) {
            $data = $data['hits']['hits'];
        }
        foreach ($data as $item) {
           $result[$item['_id']] = $item['_source'];
        }

        return $result;
    }
}