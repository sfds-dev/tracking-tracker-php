<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace TrackingTracker;


class TrackingConverter {
	public function convert($data) {
        $result = array();

        if (!$data) {
            return $result;
        }

        if (array_key_exists('hits', $data)) {
        	$data = $data['hits']['hits'];
        }
        foreach ($data as $item) {
        	if (array_key_exists('tracking', $item['_source'])) {
        		$tracking = $item['_source']['tracking'];
        	} else {
        		$tracking = null;
        	}

        	$result[$item['_id']] = array(
        		'order_id' => $item['_source']['order_id'],
        		'tracking' => $tracking,
        		'time' => $item['_source']['time']
        	);
        }

        return $result;
    }
}
