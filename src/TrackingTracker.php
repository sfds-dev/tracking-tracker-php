<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace TrackingTracker;

use Elasticsearch\ClientBuilder;

class TrackingTracker {
	private $es;
	private $converter;
    private $indiceName;
    private $statusIndiceName;
    private $statusConverter;
    private $trackedQuery;
    private $untrackedQuery;

    public function __construct($host, $port, $user, $password) {
    	$builder = ClientBuilder::create();
    	$hostStr = $host . ':' . $port;
    	if ($user && $password) {
    		$hostStr = $user . ':' . $password . '@' . $hostStr;
    		// $builder->setBasicAuthentication($user, $password);
    	}
    	$builder->setHosts([$hostStr]);
    	$builder->setRetries(12);
    	$this->es = $builder->build();

    	$this->converter = new TrackingConverter();
        $this->statusConverter = new TrackingStatusConverter();
    	$this->indiceName = 'trackings';
        $this->statusIndiceName = 'tracking_status';
    	$this->trackedQuery = array(
    		'query'=> array(
    			'exists' => array(
    				'field' => 'tracking'
    			)
    		)
	    );
	    $this->untrackedQuery = array(
    		'query'=> array(
    			'bool' => array(
    				'must_not' => array(
		    			'exists' => array(
		    				'field' => 'tracking'
		    			)
    				)
    			)
    		)
	    );
    }

    public function saveTrackings($trackings) {
        $now = new \DateTime();
        $curTimeStr = $now->format('Y-m-d') . 'T' . $now->format('H:i:s');
        $params = ['body' => []];
        foreach ($trackings as $orderId => $tracking) {
            $params['body'][] = array(
                'index' => array(
                    '_index' => $this->indiceName,
                    '_type' => '_doc',
                    '_id' => $orderId,
                )
            );
            $params['body'][] = array(
                'order_id' => $orderId,
                'tracking' => $tracking,
                'time' => $curTimeStr
            );
        }

        return $this->es->bulk($params);
    }

    public function getTrackings($orderIds) {
    	$trackings = array();

    	$params = array(
    		'index' => $this->indiceName,
    		'from' => 0,
    		'size' => count($orderIds),
    		'body' => array(
    			'query' => array('terms' => ['_id' => $orderIds])
    		)
    	);
    	$resp = $this->es->search($params);
    	$result = $this->converter->convert($resp);
    	foreach ($orderIds as $orderId) {
    		if (array_key_exists($orderId, $result)) {
    			$trackings[$orderId] = $result[$orderId];
    		} else {
    			$trackings[$orderId] = null;
    		}
    	}

    	return $trackings;
    }

    public function listTrackings($startDate = null, $endDate = null) {
        $rangeCond = array(
            'range' => ['time' => ['format' => 'dd/MM/yyyy']]
        );
        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['exists' => ['field' => 'tracking']],
                    ]
                ]
            ]
        ];
        if ($startDate) {
            $rangeCond['range']['time']['gte'] = $startDate;
        } else {
            $rangeCond['range']['time']['gte'] = '01/01/1970';
        }

        if ($endDate) {
            $rangeCond['range']['time']['lte'] = $endDate;
        } else {
            $now = new \DateTime();
            $rangeCond['range']['time']['lte'] = $now->format('d/m/Y');
        }
        array_push($query['query']['bool']['must'], $rangeCond);

        foreach ($this->scan($this->indiceName, $query) as $item) {
            yield $item['_source'];
        }
    }

    public function getTrackingStatuses($trackings) {
        $trackingStatuses = array();

        $params = array(
            'index' => $this->statusIndiceName,
            'from' => 0,
            'size' => count($trackings),
            'body' => array(
                'query' => array('terms' => ['_id' => $trackings])
            )
        );
        $resp = $this->es->search($params);
        $result = $this->statusConverter->convert($resp);
        foreach ($trackings as $tracking) {
            if (array_key_exists($tracking, $result)) {
                $trackingStatuses[$tracking] = $result[$tracking];
            } else {
                $trackingStatuses[$tracking] = null;
            }
        }

        return $trackingStatuses;
    }

    public function getTrackedOrders() {
    	foreach ($this->searchOrderIds($this->trackedQuery) as $orderId) {
    		yield $orderId;
    	}
    }

    public function getUntrackedOrders() {
    	foreach ($this->searchOrderIds($this->untrackedQuery) as $orderId) {
    		yield $orderId;
    	}
    }

    public function getOrdersCount() {
        return $this->countOrders(['query' => ['match_all' => (object)[]]]);
    }

    public function getUntrackedOrdersCount() {
        return $this->countOrders($this->untrackedQuery);
    }

    public function getTrackedOrdersCount() {
        return $this->countOrders($this->trackedQuery);
    }

    public function countOrders($query) {
        $params = array(
        	'index' => $this->indiceName,
        	'body' => $query
        );

        $result = $this->es->count($params);
        if (is_array($result) && array_key_exists('count', $result)) {
        	$count = $result['count'];
        } else {
        	$count = 0;
        }

        return $count;
    }

    public function searchOrderIds($query) {
    	foreach ($this->scan($this->indiceName, $query) as $item) {
    		yield $item['_id'];
    	}
    }

    public function scan(
    	string $index,
    	array $query = [],
    	string $scroll="5m",
    	bool $raiseOnError = true,
    	bool $preserveOrder = false,
    	int $size = 1000,
    	bool $clearScroll = true,
    	array $scrollParams = []) {
    	if ($preserveOrder) {
    		$query['sort'] = '_doc';
    	}

    	$params = array(
    		'index' => $index,
    		'body' => $query,
    		'scroll' => $scroll,
    		'size' => $size
    	);
    	$resp = $this->es->search($params);

    	$scrollId = $resp['_scroll_id'];
    	try {
    		while ($scrollId && $resp['hits']['hits']) {
    			foreach ($resp['hits']['hits'] as $hit) {
    				yield $hit;
    			}

    			if ($resp['_shards']['successful'] + $resp['_shards']['skipped'] < $resp['_shards']['total']) {
    				if ($raiseOnError) {
    					throw new Exception('Scroll request has only succeeded on ' . $resp['_shards']['successful'] . '(+' . $resp['_shards']['skipped'] . ' skiped) shards out of ' . $resp['_shards']['total'] . '.');
    				}
    			}

    			$resp = $this->es->scroll(array(
    				'scroll_id' => $scrollId,
    				'scroll' => $scroll
    			));
            	$scrollId = $resp["_scroll_id"];
    		}
    	} finally {
    		if ($scrollId && $clearScroll) {
    			$this->es->clearScroll(array(
    				'body' => array(
    					'scroll_id' => [$scrollId]
    				)
    			));
    		}
    	}
    }
}